<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */

namespace R1c0ContactModule\Form;

use R1c0ContactModule\InputFilter\Contact as ContactFilter;
use Zend\Form\Form;

class Contact extends Form
{
	public function __construct()
	{
		parent::__construct( 'contact' );
		
		$this->setAttribute( 'method', 'post' );
		$this->setAttribute( 'accept-charset', 'utf-8' );
		
		$titleValueOptions = array(
			'Mr.',
			'Mrs.',
		);
		
		$this->setInputFilter( new ContactFilter(
				array( 'title_haystack' => array_keys(
						$titleValueOptions
					)
				)
			)
		);
		
		$this->add( array(
				'name' => 'title',
				'type' => 'Zend\Form\Element\Select',
				'attributes' => array(
					'id' => 'title',
					'class' => 'form-control',
				),
				'options' => array(
					'label' => 'Title',
					'empty_option' => 'Please select',
					'value_options' => $titleValueOptions,
				),
			)
		);
		
		$this->add( array(
				'name' => 'name',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array(
					'id' => 'name',
					'class' => 'form-control',
					'placeholder' => 'Your name',
				),
				'options' => array(
					'label' => 'Name',
				),
			)
		);
		
		$this->add( array(
				'name' => 'phone',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array(
					'id' => 'phone',
					'class' => 'form-control',
					'placeholder' => 'Your phone number',
				),
				'options' => array(
					'label' => 'Phone',
				),
			)
		);
		
		$this->add( array(
				'name' => 'email',
				'type' => 'Zend\Form\Element\Text',
				'attributes' => array(
					'id' => 'email',
					'class' => 'form-control',
					'placeholder' => 'Your e-mail address',
				),
				'options' => array(
					'label' => 'E-Mail',
				),
			)
		);
		
		$this->add( array(
				'name' => 'message',
				'type' => 'Zend\Form\Element\Textarea',
				'attributes' => array(
					'id' => 'message',
					'class' => 'form-control',
					'placeholder' => 'Your message',
					'cols' => 30,
					'rows' => 3,
				),
				'options' => array(
					'label' => 'Message',
				),
			)
		);
		
		$this->add( array(
				'name' => 'csrf',
				'type' => 'Zend\Form\Element\Csrf',
			)
		);
		
		$this->add( array(
				'name' => 'send',
				'type' => 'Zend\Form\Element\Button',
				'attributes' => array(
					'type' => 'submit',
					'class' => 'btn btn-default',
				),
				'options' => array(
					'label' => 'Submit',
				),
			)
		);
	}
}
