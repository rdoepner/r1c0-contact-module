<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */

namespace R1c0ContactModule\Controller;

use R1c0ContactModule\Options\ModuleOptions;
use R1c0ContactModule\Form\Contact as ContactForm;
use R1c0ContactModule\Exception\RuntimeException;
use R1c0BaseModule\Mailer\MailerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Validator\AbstractValidator;

class IndexController extends AbstractActionController
{
	const EVENT_SEND_PRE = "send.pre";
	const EVENT_SEND_POST = "send.post";
	
	const ROUTE_SENT = "contact/sent";
	
	protected $moduleOptions;
	
	protected $contactForm;
	
	protected $mailer;
	
	public function __construct( ModuleOptions $moduleOptions, ContactForm $contactForm, MailerInterface $mailer )
	{
		$this->setModuleOptions( $moduleOptions );
		$this->setContactForm( $contactForm );
		$this->setMailer( $mailer );
	}
	
	public function setModuleOptions( ModuleOptions $moduleOptions )
	{
		$this->moduleOptions = $moduleOptions;
		
		return $this;
	}
	
	public function getModuleOptions()
	{
		return $this->moduleOptions;
	}
	
	public function setContactForm( $contactForm )
	{
		$this->contactForm = $contactForm;
		
		return $this;
	}
	
	public function getContactForm()
	{
		return $this->contactForm;
	}
	
	public function setMailer( MailerInterface $mailer )
	{
		$this->mailer = $mailer;
		
		return $this;
	}
	
	public function getMailer()
	{
		return $this->mailer;
	}
	
	public function indexAction()
	{
		$contactForm = $this->getContactForm();
		$moduleOptions = $this->getModuleOptions();
		$recipients = $moduleOptions->getRecipients();
		
		if( !count( $recipients ) )
		{
			throw new RuntimeException( 'There must be at least one recipient.' );
		}
		
		$request = $this->getRequest();
		
		if( $request->isPost() )
		{
			AbstractValidator::setDefaultTranslatorTextDomain( 'r1c0/contact' );
			
			$contactForm->setData( $request->getPost() );
			
			if( $contactForm->isValid() )
			{
				$validated = $contactForm->getData();
				
				$mailer = $this->getMailer();
				$mailer->addTo( $recipients );
				$mailer->setSender( $moduleOptions->getSender() );
				$mailer->setFrom( $validated['email'], $validated['name'] );
				$mailer->setReplyTo( $validated['email'], $validated['name'] );
				
				$subject = $moduleOptions->getSubject();
				$matches = array();
				
				if( preg_match_all( "#%(.*?)%#", $subject, $matches, PREG_SET_ORDER ) )
				{
					for( $i = 0; $i < count( $matches ); $i++ )
					{
						if( array_key_exists( $matches[$i][1], $validated ) )
						{
							$subject = str_replace( $matches[$i][0], $validated[$matches[$i][1]], $subject );
						}
					}
				}
				
				$mailer->setSubject( $subject );
				
				!empty( $validated['phone'] ) || $validated['phone'] = "n/a";
				
				$mailBodyTemplate = new ViewModel( array( 'values' => $validated ) );
				$mailBodyTemplate->setTemplate( 'r1c0-contact-module/mail/plaintext' );
				$viewRenderer = $this->getServiceLocator()->get( 'ViewRenderer' );
				$text = $viewRenderer->render( $mailBodyTemplate );
				$mailer->setTextBody( $text );
				
				$this->getEventManager()->trigger( self::EVENT_SEND_PRE, $this, array(
						'mailer' => $mailer
					)
				);
				
				$status = $mailer->send() ? "success" : "fatal";
				
				$this->getEventManager()->trigger( self::EVENT_SEND_POST, $this, array(
						'mailer' => $mailer
					)
				);
				
				return $this->redirect()->toRoute(
					self::ROUTE_SENT, array(
						'status' => $status
					)
				);
			}
		}
		
		return new ViewModel( array(
				'elementErrors' => array_keys( $contactForm->getMessages() ),
				'isPost' => $request->isPost(),
				'form' => $contactForm
			)
		);
	}
	
	public function sentAction()
	{
		$model = new ViewModel();
		$model->setTemplate( sprintf( 'r1c0-contact-module/index/%s',
				$this->params( 'status', 'fatal' )
			)
		);
		
		return $model;
	}
}
