<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */

namespace R1c0ContactModule\InputFilter;

use R1c0ContactModule\Exception\InvalidArgumentException;
use Zend\InputFilter\InputFilter;

class Contact extends InputFilter
{
	const MSG_REQUIRED_FILED = "This field is required";
	const MSG_EMAIL_NOT_VALID = "This e-mail is not valid";
	const MSG_TITLE_NOT_ALLOWED = "The selected title is not allowed";
	
	public function __construct( array $options )
	{
		if( !array_key_exists( 'title_haystack', $options ) )
		{
			throw new InvalidArgumentException( 'Haystack for title validation not set.' );
		}
		
		$this->add( array(
				'name' => 'title',
				'required' => true,
				'validators' => array(
					array(
						'name' => 'Zend\Validator\InArray',
						'break_chain_on_failure' => true,
						'options' => array(
							'haystack' => $options['title_haystack'],
							'messages' => array(
								'notInArray' => self::MSG_TITLE_NOT_ALLOWED,
							),
						),
					),
					array(
						'name' => 'Zend\Validator\NotEmpty',
						'options' => array(
							'messages' => array(
								'notEmptyInvalid' => self::MSG_REQUIRED_FILED,
								'isEmpty' => self::MSG_REQUIRED_FILED,
							),
						),
					),
				),
			)
		);
		
		$this->add( array(
				'name' => 'name',
				'required' => true,
				'filters' => array(
					array(
						'name' => 'Zend\Filter\StringTrim',
					),
				),
				'validators' => array(
					array(
						'name' => 'Zend\Validator\NotEmpty',
						'options' => array(
							'messages' => array(
								'notEmptyInvalid' => self::MSG_REQUIRED_FILED,
								'isEmpty' => self::MSG_REQUIRED_FILED,
							),
						),
					),
				),
			)
		);
		
		$this->add( array(
				'name' => 'email',
				'required' => true,
				'filters' => array(
					array(
						'name' => 'Zend\Filter\StringTrim',
					),
				),
				'validators' => array(
					array(
						'name' => 'Zend\Validator\NotEmpty',
						'break_chain_on_failure' => true,
						'options' => array(
							'messages' => array(
								'notEmptyInvalid' => self::MSG_REQUIRED_FILED,
								'isEmpty' => self::MSG_REQUIRED_FILED,
							),
						),
					),
					array(
						'name' => 'Zend\Validator\EmailAddress',
						'options' => array(
							'messages' => array(
								'emailAddressInvalid' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressInvalidFormat' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressInvalidHostname' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressInvalidMxRecord' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressInvalidSegment' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressDotAtom' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressQuotedString' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressInvalidLocalPart' => self::MSG_EMAIL_NOT_VALID,
								'emailAddressLengthExceeded' => self::MSG_EMAIL_NOT_VALID,
								'hostnameCannotDecodePunycode' => self::MSG_EMAIL_NOT_VALID,
								'hostnameInvalid' => self::MSG_EMAIL_NOT_VALID,
								'hostnameDashCharacter' => self::MSG_EMAIL_NOT_VALID,
								'hostnameInvalidHostname' => self::MSG_EMAIL_NOT_VALID,
								'hostnameInvalidHostnameSchema' => self::MSG_EMAIL_NOT_VALID,
								'hostnameInvalidLocalName' => self::MSG_EMAIL_NOT_VALID,
								'hostnameInvalidUri' => self::MSG_EMAIL_NOT_VALID,
								'hostnameIpAddressNotAllowed' => self::MSG_EMAIL_NOT_VALID,
								'hostnameLocalNameNotAllowed' => self::MSG_EMAIL_NOT_VALID,
								'hostnameUndecipherableTld' => self::MSG_EMAIL_NOT_VALID,
								'hostnameUnknownTld' => self::MSG_EMAIL_NOT_VALID,
							),
						),
					),
				),
			)
		);
		
		$this->add( array(
				'name' => 'message',
				'required' => true,
				'filters' => array(
					array(
						'name' => 'Zend\Filter\StringTrim',
					),
				),
				'validators' => array(
					array(
						'name' => 'Zend\Validator\NotEmpty',
						'options' => array(
							'messages' => array(
								'notEmptyInvalid' => self::MSG_REQUIRED_FILED,
								'isEmpty' => self::MSG_REQUIRED_FILED,
							),
						),
					),
				),
			)
		);
	}
}
